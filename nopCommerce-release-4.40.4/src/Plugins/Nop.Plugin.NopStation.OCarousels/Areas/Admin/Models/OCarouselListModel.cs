﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.NopStation.OCarousels.Areas.Admin.Models
{
    public record OCarouselListModel : BasePagedListModel<OCarouselModel>
    {
    }
}