﻿namespace Nop.Plugin.NopStation.OCarousels.Models
{
    public enum CarouselType
    { 
        Product,
        Manufacturer,
        Category,
        Vendor
    }
}
