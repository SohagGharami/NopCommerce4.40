﻿namespace Nop.Plugin.Misc.ZimzoneAdditionalFeature
{
    public static class ScriptPaths
    {
        public static string ImportTablesCreateSqlFilePath => "~/Plugins/Misc.ZimzoneAdditionalFeature/DBScripts/SqlServer.ImportTable.Create.sql";
        public static string ImportTablesDropSqlFilePath => "~/Plugins/Misc.ZimzoneAdditionalFeature/DBScripts/SqlServer.ImportTable.Drop.sql";
        public static string ImportStoredProceduresCreateSqlFilePath => "~/Plugins/Misc.ZimzoneAdditionalFeature/DBScripts/SqlServer.StoredProcedures.Create.sql";
        public static string ImportStoredProceduresDropSqlFilePath => "~/Plugins/Misc.ZimzoneAdditionalFeature/DBScripts/SqlServer.StoredProcedures.Drop.sql";

        public static string CustomOrderStatusCreateSqlFilePath => "~/Plugins/Misc.ZimzoneAdditionalFeature/DBScripts/SqlServer.CustomOrderStatusTable.Create.sql";
        public static string CustomOrderStatusDropSqlFilePath => "~/Plugins/Misc.ZimzoneAdditionalFeature/DBScripts/SqlServer.CustomOrderStatusTable.Drop.sql";

        public static string OrderWithCustomStatusCreateSqlFilePath => "~/Plugins/Misc.ZimzoneAdditionalFeature/DBScripts/SqlServer.OrderWithCustomStatusTable.Create.sql";
        public static string OrderWithCustomStatusDropSqlFilePath => "~/Plugins/Misc.ZimzoneAdditionalFeature/DBScripts/SqlServer.OrderWithCustomStatusTable.Drop.sql";

        public static string ErpManufacturerCreateSqlFilePath => "~/Plugins/Misc.ZimzoneAdditionalFeature/DBScripts/SqlServer.ErpManufacturerTable.Create.sql";
        public static string ErpManufacturerDropSqlFilePath => "~/Plugins/Misc.ZimzoneAdditionalFeature/DBScripts/SqlServer.ErpManufacturerTable.Drop.sql";
    }
}
