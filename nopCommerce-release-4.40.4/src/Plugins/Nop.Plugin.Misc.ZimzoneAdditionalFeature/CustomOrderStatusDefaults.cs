﻿namespace Nop.Plugin.Misc.ZimzoneAdditionalFeature
{
    public static class CustomOrderStatusDefaults
    {
        public static string OrderStatusBottom => "OrderStatusBottom";
        public static string MessageTemplateName => "OrderStatus.CustomOrderStatusMessage";
    }
}
