﻿namespace Nop.Plugin.Misc.ZimzoneAdditionalFeature
{
    public static class GiftVoucherDefaults
    {
        public static string CUSTOM_WIDGET_GIFT_CARD => "productdetails_replace_gift_card";
        public static string USD_CURRENCY_CODE => "USD";
        public static string CUSTOM_WIDGET_GIFT_VOUCHER_DELIVERYDATE => "productdetails_addtocart_delivery_date";
        public static string CUSTOM_WIDGET_GIFT_VOUCHER_CHANGE_TO_USD_MESSAGE => "electrosales_credit_voucher_currency_change_message";
        public static string CUSTOM_WIDGET_GIFT_VOUCHER_CHANGE_TO_USD_BUTTON => "electrosales_credit_voucher_currency_change_button";
        public static string CUSTOM_WIDGET_GIFT_VOUCHER_PRICE_SELCT_BUTTONS => "productdetails_addtocart_price_buttons_gift_card";
        public static string CUSTOM_WIDGET_ZONE_MEGA_MENU_BEFORE_ITEMS => "custom_header_menu_before";
        public static string ElectrosalesCreditVoucher_CustomerNotification => "ElectrosalesCreditVoucher.CustomerNotification";
    }
}
