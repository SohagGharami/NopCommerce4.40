﻿namespace Nop.Plugin.Misc.ZimzoneAdditionalFeature
{
    public static class ServiceRequestTemplateSystemNames
    {
        public static string ServiceRequestSubmitted => "RequestSubmitted.CustomerNotification";
        public static string ServiceRequestAccepted => "RequestAccepted.CustomerNotification";

        public static string QuerySubmittedCustomerNotification => "QuerySubmitted.CustomerNotification";

        public static string QuerySubmittedAdminNotification => "QuerySubmitted.AdminNotification";
    }
}
