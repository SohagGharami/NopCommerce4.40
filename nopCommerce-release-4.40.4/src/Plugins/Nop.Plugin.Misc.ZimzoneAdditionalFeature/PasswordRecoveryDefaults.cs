﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.ZimzoneAdditionalFeature
{
    public static class PasswordRecoveryDefaults
    {
        public static string PasswordRecoveryKey => "Misc.ZimzoneAdditionalFeature.PasswordRecovery.Key";
    }
}
