﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Misc.ZimzoneAdditionalFeature.Areas.Admin.Models.CustomOrderStatus
{
    public record CustomOrderStatusListModel : BasePagedListModel<CustomOrderStatusModel>
    {
    }
}
