﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Misc.ZimzoneAdditionalFeature.Areas.Admin.Models.Query
{
    public record QuestionListModel : BasePagedListModel<QuestionModel>
    {
    }
}
