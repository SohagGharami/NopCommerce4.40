﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Misc.ZimzoneAdditionalFeature.Areas.Admin.Models.PriceUpdate
{
    public record PriceUpdateErrorListModel : BasePagedListModel<PriceUpdateErrorModel>
    {
    }
}
