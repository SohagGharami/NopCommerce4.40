﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Misc.ZimzoneAdditionalFeature.Areas.Admin.Models.Voucher
{
    public record ElectrosalesVoucherListModel : BasePagedListModel<ElectrosalesVoucherModel>
    {
    }
}
