﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Misc.ZimzoneAdditionalFeature.Areas.Admin.Models
{
    public record ServiceRequestListModel : BasePagedListModel<ServiceRequestModel>
    {
    }
}
