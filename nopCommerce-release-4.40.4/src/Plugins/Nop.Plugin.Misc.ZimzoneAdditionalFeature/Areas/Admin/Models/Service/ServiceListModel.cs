﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Misc.ZimzoneAdditionalFeature.Areas.Admin.Models
{
    public record ServiceListModel : BasePagedListModel<ZimzoneServiceModel>
    {
    }
}
