﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nop.Plugin.Misc.ZimzoneAdditionalFeature
{
    public static class AdditionalFeatureDefaults
    {
        /// <summary>
        /// Represents system name of notification admin about shipment created
        /// </summary>
        public static string ShipmentCreatedAdminNotification => "ShipmentCreated.AdminNotification";
    }
}
