﻿namespace Nop.Plugin.NopStation.Core.Helpers
{
    public enum PluginType
    {
        PaymentMethod = 10,
        ShippingRateComputationMethod = 20,
        PickupPointProvider = 30,
        ExternalAuthenticationMethod = 40,
        WidgetPlugin = 50,
        TaxProvider = 60,
        MultiFactorAuthenticationMethod = 70
    }
}
