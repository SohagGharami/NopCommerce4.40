﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nop.Core;

namespace Nop.Plugin.NopStation.Core.Domains
{
    public class License : BaseEntity
    {
        public string Key { get; set; }
    }
}
