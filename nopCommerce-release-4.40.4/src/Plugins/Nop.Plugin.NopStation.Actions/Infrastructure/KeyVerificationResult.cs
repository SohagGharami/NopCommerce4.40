﻿namespace Nop.Plugin.NopStation.Core.Infrastructure
{
    public enum KeyVerificationResult
    {
        InvalidProductKey,
        InvalidProduct,
        InvalidForDomain,
        InvalidForNOPVersion,
        Valid
    }
}
